FROM python:3.8.0-slim-buster

COPY . .
COPY .git ./.git
RUN apt update && apt install -y git
RUN pip install -r requirements.txt
CMD [ "python", "main.py" ]