#    This file is part of BioBot
#    Copyright (C) 2018 Penn Mackintosh
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

from telethon import TelegramClient
from telethon.tl.functions.users import GetFullUserRequest
from telethon.tl.types import InputChannel, InputUser
from telethon.errors import FloodWaitError

import re, atexit, traceback, asyncio

from api_token import API_TOKEN, API_TOKEN_USER, API_ID, PHONE
from constants import *

client = TelegramClient("BioBot", API_ID, API_TOKEN_USER)

find_usernames = re.compile(r'@[_0-9A-Za-z]*', flags=re.MULTILINE)

global setup
setup = False

async def init():
    await client.start(phone=PHONE)
    await client.get_dialogs()
    client.flood_sleep_threshold = 0
    atexit.register(deinit)

def deinit():
    pass

async def list_group(group_id):
    global setup
    if not setup:
        await init()
        setup=True
    x = await client.get_participants(group_id)
    return x

async def get_bio_nocache(id):
    global setup
    if not setup:
        await init()
        setup=True
    try:
        x = await client(GetFullUserRequest(id))
    except FloodWaitError as e:
        await asyncio.sleep(e.seconds)
        x = await client(GetFullUserRequest(id))
    bio = x.about
    if bio:
        return find_usernames.findall(bio)
    else:
        return []

async def get_bio(username, cache={}):
    global setup
    if not setup:
        await init()
        setup=True
    if username.lower() in cache:
        x = await client(GetFullUserRequest(InputUser(user_id=cache[username.lower()].id, access_hash=cache[username.lower()].access_hash)))
    else:
        return []
    bio = x.about
    if bio:
        return find_usernames.findall(bio)
    else:
        return []
