#    This file is part of BioBot
#    Copyright (C) 2018 Penn Mackintosh
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import time, sys, copy, functools, subprocess, asyncio

from aiogram import Bot, Dispatcher, executor, types
from aiogram.types.message import ContentType
from aiogram.utils.markdown import bold, escape_md, text
from aiogram.types import ParseMode, ChatType, ChatMemberStatus
from aiogram.utils.exceptions import MessageNotModified

import aiohttp
from lxml import html
import tree_format

from constants import *
from api_token import API_TOKEN
from diff import diff

import userbot_helper
from telethon.errors import FloodWaitError


bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)

sys.setrecursionlimit(700)

global session
session = None

global blockeduntil
blockeduntil = 0

async def get_usernames_1(username, usercache={}):
    global session
    if not username.lower() in [a.lower() for a in usercache]:
        return [] #Concistency... get_usernames_2 does this and they should have the same return value, always!
    async with session.get('https://t.me/'+username) as resp:
        assert resp.status == 200
        text = await resp.text()
        tree = html.fromstring(text)
        invalid = tree.xpath('//div[@class="tgme_page_description"]/a[@class="tgme_username_link"]') #this won't match on a user that exists but will match on an invalid username.
        name = tree.xpath('//div[@class="tgme_page_title"]')
        if len(invalid) > 0:
            # This happens if the username doesnt exist, OR we're being rate-limited. Let's check with the API which will tell us if we are rate limited or not.
            usernames = []
            try:
                if time.time() <= blockeduntil:
                    await asyncio.sleep(blockeduntil - time.time())
                usernames = await get_usernames_2(username, usercache)
            except FloodWaitError as e:
                await asyncio.sleep(e.seconds) #We're rate limited in the main API too, but we don't want to return the wrong thing
                try:
                    usernames = await userbot_helper.get_bio(username, usercache)
                except:
                    pass # Easy failout
            return usernames
        usernames = tree.xpath('//div[@class="tgme_page_description "]/a[starts-with(@href, "https://t.me/")]/text()')
        return usernames

async def get_usernames_2(username, usercache={}):
    usernames = await userbot_helper.get_bio(username, usercache)
    return usernames

async def scrape_bio_tree(starting_username, userlist={}, skip=[], usercache={}):
    global blockeduntil
    ret = {}
    if time.time() > blockeduntil:
        try:
            usernames = await get_usernames_2(starting_username, usercache)
        except FloodWaitError as e:
            blockeduntil = time.time() + e.seconds
    if not time.time() > blockeduntil:
        usernames = await get_usernames_1(starting_username, usercache)
    if len(usernames) == 0:
        return {"@"+starting_username: {}}
    ret["@"+starting_username] = {}
    for username in usernames:
        if username.lower() in skip:
            print("Recursion detected in the chain at "+username+" from @"+starting_username)
            continue
        elif username.lower() in [k.lower() for k in listkeys(userlist)[0]]:
            ret["@"+starting_username].update(copy.deepcopy(find(userlist, username)))
        elif username.lower() == "@bio_chain_2":
            print("Found end of chain!")
            ret["@"+starting_username].update({"@bio_chain_2": {}})
        else:
            recurse = await scrape_bio_tree(username[1:], userlist, skip+["@"+starting_username.lower()], usercache)
            ret["@"+starting_username].update(recurse)
    return ret

def find(userlist, target):
    for key in userlist:
        if key.lower() == target.lower():
            return {key: userlist[key]}
        elif isinstance(userlist[key], dict):
            x = find(userlist[key], target)
            if x:
                return x

def listkeys(dicty, target="@bio_chain_2"):
    keys = []
    tracking = []
    for key in dicty:
        keys += [key]
        if isinstance(dicty[key], dict):
            x = listkeys(dicty[key], target)
            keys += x[0]
            if key.lower() == target.lower():
                tracking = [key]
            if x[1] != []:
                tracking = [key] + tracking
    return [keys, tracking]

def cleanup(x):
    x = copy.deepcopy(x)
    dele = []
    for key1 in x:
        for key2 in x:
            if key1.lower() in [z.lower() for z in listkeys(x[key2])[0]] and key1.lower() != key2.lower(): #key is a subkey of another one
                dele += [key1]
    for key in set(dele):
        del x[key]
    return x

async def update_chain():
    import time
    stime = time.time()
    print("start chain update!")
    usersdone = list()
    userlist = dict()
    usercache = await userbot_helper.list_group(MAIN_ID)
    usercache = {x.username.lower(): x for x in usercache if x.username}
    userstmp = list(usercache.keys())
    lastuserstmp = len(userstmp)
    global session
    if session == None:
        session = aiohttp.ClientSession()
    while len(userstmp) > 0:
        user = userstmp.pop()
        if not "@"+user.lower() in [x.lower() for x in usersdone]:
            x = await scrape_bio_tree(user, copy.deepcopy(userlist), usercache=usercache)
            if len(x) > 0:
                userlist.update(copy.deepcopy(x))
                usersdone += listkeys(x)[0]
        userstmp = [e for e in userstmp if not e.lower() in [a.lower() for a in usersdone]]
        lastuserstmp = len(userstmp)
    print("took "+str(time.time()-stime)+"s")
    return cleanup(userlist)













@dp.message_handler(content_types=ContentType.NEW_CHAT_MEMBERS)
async def new_member(message: types.Message):
    if message.chat.id == ADMISSION_ID:
        await new_member_admission(message)
    elif message.chat.id == MAIN_ID:
        await new_member_main(message)
        await chain_command(message)
    else:
        print("Skipping message from "+str(message.chat.id))
async def new_member_admission(message: types.Message):
    for member in message.new_chat_members:
        print("Adding member: "+str(member.id)+"; username="+member.username or "")
        if member.username == "":
            await bot.send_message(message.chat.id, message.mention+GET_USERNAME)
        else:
            await bot.send_message(message.chat.id, member.mention+PM_BOT)
async def new_member_main(message: types.Message):
    for member in message.new_chat_members:
        print("Adding member: "+str(member.id)+"; username="+member.username)
        if member.username == "":
            print("Invite link leaked!")
            await message.reply(INVITE_LEAK)
        print("revoking invite link")
        await bot.export_chat_invite_link(MAIN_ID)


@dp.message_handler(content_types=ContentType.LEFT_CHAT_MEMBER)
async def left_member(message: types.Message):
    if message.chat.id == MAIN_ID:
        print("Someone left, strategic analysis...")
        x = await update_chain()
        y = find_chain_path(x, "@bio_chain_2")
        z = y.index(message.user.username)
        await bot.send_message(message.chat.id, "@"+escape_md(y[z-1])+", please change your bio to point at "+y[z+1])

def convert_a(x, b=[]):
    return format(x[0], b=b)
def convert_b(x):
    return [[k, x[1][k]] for k in x[1]]

def format(x, b=[]):
    if x in b:
        return bold(x[1:])
    else:
        return escape_md(x[1:])




def find_chain_path(chain, target):
    path = []
    ret = []
    for key in chain:
        if key.lower() == target.lower() and len(ret) < 1:
            ret = [key]
        else:
            x = find_chain_path(chain[key], target)
            if len(x) > 0 and len(ret) < len(x)+1:
                ret = [key] + x
    return ret

@dp.message_handler(commands=["whereami"])
async def whereami_command(message: types.Message):
    if message.chat.id == MAIN_ID or message.chat.id == BOT_ID:
        z = message.get_args() or ("@"+message["from"].username)
        print(z)
        x = await update_chain()
        y = [b.lower() for b in find_chain_path(x, "@bio_chain_2")]
        print(z, y)
        try:
            a = len(y) - y.index(z.lower())
        except:
            a = "You're not in the chain!"
        await message.reply(str(a))

@dp.message_handler(commands=["diff"])
async def diff_command(message: types.Message):
    if message.chat.id == MAIN_ID or message.chat.id == BOT_ID:
        if message.reply_to_message:
            z = message.reply_to_message.text.split('\n')[-1]
        else:
            z = message.get_args()
        z = z.split(' ⇒ ')
        x = await update_chain()
        y = [b[1:] for b in find_chain_path(x, "@bio_chain_2")]
        a = diff(z, y)
        print(DIFF_OUTPUT.format(', '.join(a[0]),', '.join(a[1]),', '.join(a[2])))
        await message.reply(DIFF_OUTPUT.format(', '.join(a[0]),', '.join(a[1]),', '.join(a[2])))

@dp.message_handler(commands=["ping"])
async def ping_command(message: types.Message):
    await message.reply("Pong!")

@dp.message_handler(commands=['chain'])
async def chain_command(message: types.Message):
    if message.chat.id == MAIN_ID or message.chat.id == BOT_ID:
        x = await update_chain()
        print('keys are',x.keys())
        y = find_chain_path(x, "@bio_chain_2")
        m = (await bot.get_chat(message.chat.id)).pinned_message
        if m and m.text and m.text[0:6] == "#chain":
            if int(m.text.split('\n', 1)[0][6:]) > len(y):
                await bot.send_message(message.chat.id, "Refusing to make the pinned message shorter; chain follows.")
                await bot.send_message(message.chat.id, "#chain"+str(len(y))+"\n\nChain:\n\n"+" ⇒ ".join([z[1:] for z in y]))
                return
            try:
                await m.edit_text("#chain"+str(len(y))+"\n\nChain:\n\n"+" ⇒ ".join([z[1:] for z in y]))
            except MessageNotModified:
                await bot.send_message(message.chat.id, "Nothing changed.")
            except:
                await bot.send_message(message.chat.id, "Hmm, an error occured editing the pinned message. Debug details follow:\n```"+traceback.format_exc()+"```")
                await bot.send_message(message.chat.id, "#chain"+str(len(y))+"\n\nChain:\n\n"+" ⇒ ".join([z[1:] for z in y]))
        else:
            await bot.send_message(message.chat.id, "#chain"+str(len(y))+"\n\nChain:\n\n"+" ⇒ ".join([z[1:] for z in y]))

@dp.message_handler(commands=["version"])
async def version_command(message: types.Message):
    await message.reply("This is revision "+subprocess.check_output(["git", "rev-parse", "HEAD"]).decode('utf8')+(" and there are uncommited changes." if subprocess.call(["git", "diff-index", "--quiet", "HEAD"]) else " and the working tree is clean."))

@dp.message_handler(commands=["allchains"])
async def allchains_command(message: types.Message):
    if message.chat.id == MAIN_ID or message.chat.id == BOT_ID:
        pass

@dp.message_handler(commands=["notinchain"])
async def notinchain_command(message:types.Message):
    if message.chat.id == MAIN_ID or message.chat.id == BOT_ID:
        x = await update_chain()
        y = find_chain_path(x, "@bio_chain_2")
        z = await userbot_helper.list_group(MAIN_ID)
        print(y)
        diff = []
        for user in z:
            if user.username and not "@"+user.username.lower() in [a.lower() for a in y]:
                diff += [user.username]
        #diff = [a for a in [b.username for b in z if b.username] if not a.lower() in [b.lower() for b in y]]
        await message.reply("\n".join(diff))

@dp.message_handler(commands=['tree'])
async def tree_command(message: types.Message):
    if message.chat.id == MAIN_ID or message.chat.id == BOT_ID:
        x = await update_chain()
        print('keys are',x.keys())
        y = find_chain_path(x, "@bio_chain_2")
        tree = u'Chain\n'+u'\n'.join(tree_format._format_tree(["Chain", x], format_node=functools.partial(convert_a, b=y), get_children=convert_b, options=tree_format.Options(INDENT=u'')))
        await bot.send_message(message.chat.id, "#chain"+str(len(x)), parse_mode=ParseMode.MARKDOWN)
        while len(tree) > 0:
            x = 1024
            if len(tree) >= 1024:
                while tree[x] != "\n":
                    x -= 1
            await bot.send_message(message.chat.id, tree[:x], parse_mode=ParseMode.MARKDOWN)
            tree = tree[x+1:]
    else:
        print("ignoring command")

class Typer():
    def __init__(self, chat):
        self.chat = chat
        self.typing = True
        asyncio.get_event_loop().create_task(self.stay_typing())
    async def stay_typing(self):
        if self.typing:
            await self.chat.do("typing")
            await asyncio.sleep(4)
            asyncio.get_event_loop().create_task(self.stay_typing())
    def stop_typing(self):
        self.typing = False

@dp.message_handler(content_types=ContentType.ANY)
async def generic_message(message: types.Message):
    if message.chat.id == MAIN_ID and message["from"]["username"] == "":
        await message.reply(BANNED_USERNAME)
        await message.chat.kick(message["from"].id, None)
    elif ChatType.is_private(message):
        try:
            typer = Typer(message.chat)
            user = await bot.get_chat_member(ADMISSION_ID, message["from"]["id"])
            print(user)
            print(user.status)
            if user.status in ["member", "administrator", "creator"]:
                print("checking user stats...")
                await userbot_helper.list_group("t.me/bio_chain_2") #Add the users to the tdlib database so it can find the access hash
                usernames = await userbot_helper.get_bio_nocache(user.user.id)
                await bot.send_message(message.chat.id, PLEASE_WAIT)
                x = await update_chain()
                y = find_chain_path(x, "@bio_chain_2")
                z = y[0]
                if user.user.username and len(usernames) > 0 and z.lower() in [a.lower() for a in usernames]:
                    print("user is ready to join the chain!")
                    link = await bot.export_chat_invite_link(MAIN_ID) #Really we should get the current link and use it but meh, lets revoke it twice.
                    print("got new link")
                    await bot.send_message(message.chat.id, link)
                else:
                    print("user has no or wrong username in bio", usernames, user.user.username, z)
                    await bot.send_message(message.chat.id, SET_BIO.format(z))
        finally:
            typer.stop_typing()
if __name__ == '__main__':
    print("init")
    executor.start_polling(dp, skip_updates=True)


